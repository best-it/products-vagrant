#!/usr/bin/env bash

#==============================================================================#
#title           :install_local_source.sh                                      #
#description     :This script install local plugin.                            #
#author		     :potthast <potthast@bestit-online.de>                         #
#date            :20170124                                                     #
#version         :0.1                                                          #
#include         :source /path/to/install_local_source.sh                      #
#usage           :installLocalSource                                           #
#bash_version    :4.3.11(1)-release                                            #
#==============================================================================#

function installLocalSource() {
    echo "install local source"
    DIR="/home/vagrant/www/engine/Shopware/Plugins/Community/Frontend/$1"
    if [ ! -e $DIR ];then
        mkdir $DIR
        cd $DIR
        cp /vagrant/*.{php,md,png,json,png} ./
        cp /vagrant/LICENSE ./
        cp -r /vagrant/{Classes,Commands,Components,Controllers,Files,Snippets,Views,log}/ ./
        /usr/bin/php /home/vagrant/www/bin/console sw:plugin:refresh
        /usr/bin/php /home/vagrant/www/bin/console sw:plugin:install $1
        /usr/bin/php /home/vagrant/www/bin/console sw:plugin:activate $1

        chmod -R 0777 $DIR
    fi

}
