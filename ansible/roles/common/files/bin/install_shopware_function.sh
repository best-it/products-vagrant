#!/usr/bin/env bash

#==============================================================================#
#title           :install_shopware_function.sh                                 #
#description     :This script install shopware.                                #
#author		     :potthast <potthast@bestit-online.de>                         #
#date            :20170124                                                     #
#version         :0.1                                                          #
#include         :source /path/to/install_shopware_function.sh                 #
#usage           :install '<VersionNumber>'                                    #
#bash_version    :4.3.11(1)-release                                            #
#==============================================================================#

: '
function install() {
    if [ ! -e "/home/vagrant/www/shopware.php" ]; then
        echo "install shopware"
        cd /home/vagrant/www
        echo "Download Shopware ..."
        wget -q https://github.com/shopware/shopware/archive/v$1.zip
        echo "downloaded"
        unzip v$1.zip
        rm v$1.zip
        rm -rf shopware-$1/.github
        mv -bfv shopware-$1/* shopware-$1/.[!.]* ./
        chmod -R 0777 var
        chmod -R 0777 web
        chmod -R 0777 files
        chmod -R 0777 media
        chmod -R 0777 engine/Shopware/Plugins/Community
        echo "Download Testimages ..."
        wget -q -O test_images.zip http://releases.s3.shopware.com/test_images.zip
        echo "downloaded";
        unzip test_images.zip
        cd build/
        ant write-properties -Dapp.host=local.dev.bestit-online.de -Dapp.path=/ -Ddb.name=shopware -Ddb.host=localhost -Ddb.user=root -Ddb.password=root
        ant build-unit
    fi

}

'

function install() {
    if [ ! -e "/home/vagrant/www/shopware.php" ]; then
        sw install:release --release=$1 \
        --install-dir=/home/vagrant/www \
        --db-host=localhost \
        --db-user=root \
        --db-password=root \
        --db-name=shopware \
        --shop-host=local.dev.bestit-online.de \

     fi
}