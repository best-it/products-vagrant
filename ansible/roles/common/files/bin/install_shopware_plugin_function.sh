#!/usr/bin/env bash

#==============================================================================#
#title           :install_shopware_plugin_function.sh                          #
#description     :This script install plugin and activate plugin in shopware.  #
#author		     :potthast <potthast@bestit-online.de>                         #
#date            :20170124                                                     #
#version         :0.1                                                          #
#include         :source /path/to/install_shopware_plugin_function.sh          #
#usage           : pluginInstall '<PluginName>'                                #
#bash_version    :4.3.11(1)-release                                            #
#==============================================================================#

function pluginInstall() {

    /usr/bin/php /home/vagrant/www/bin/console sw:plugin:refresh

    mapfile result < <(mysql -u root -proot shopware -se "SELECT active FROM s_core_plugins WHERE name='$1' LIMIT 1;")

    if [ ! ${result[0]#*$} -eq 1 ] ;then

        /usr/bin/php /home/vagrant/www/bin/console sw:plugin:install $1
        /usr/bin/php /home/vagrant/www/bin/console sw:plugin:activate $1

    fi
}
